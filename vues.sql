
--------------------------------------------------------------------------------

-- Vues permettant de s'assurer que les clients ont accès seulement aux informations suivantes :
--  -> pseudo (colonne Cl_pseudo)
--  -> statut (colonne Cl_statut)

CREATE OR REPLACE VIEW V_CLIENT_INFORMATION AS
SELECT Cl_pseudo, Cl_statut
FROM CLIENTS
GROUP BY Cl_pseudo, Cl_statut;

--------------------------------------------------------------------------------

-- Vues permettant au client de voir les diffrénts artistes associés aux albums (le client n'as pu accès aux 'id'):

CREATE OR REPLACE VIEW  V_ART_INFO_FOR_CLIENTS AS 
SELECT Al_nom, G_nom, G_dateCrea, Art_nom, Art_prenom, role
FROM (((ALBUMS NATURAL JOIN GROUPES) NATURAL JOIN ASSOCALBART) NATURAL JOIN ARTISTES) NATURAL JOIN COMPETENCES;

--------------------------------------------------------------------------------

-- Vues permettant à un client de prévisualiser les chansons qu'il a acheté.

CREATE OR REPLACE VIEW V_CLIENT_MUSIQUE AS
SELECT M_titre
FROM MUSICPOSS
WHERE Cl_pseudo = (SELECT USER FROM DUAL)
GROUP BY M_titre;



select * from V_CLIENT_MUSIQUE;
