/*
Pour SQL Oracle installés sur PC persos uniquement: 

DROP USER L3_44;
DROP USER L3_48;

CREATE USER L3_44 IDENTIFIED BY pass;
GRANT CREATE SESSION TO L3_44;

CREATE USER L3_48 IDENTIFIED BY pass;
GRANT CREATE SESSION TO L3_48;

Problème : ces utilisateurs n'ont pas accès à la base de donnée donc ne voient pas les vues non plus
Je n'ai pas encore trouvé de solution pour résoudre ce problème

*/

--Suppression des différents rôles
DROP ROLE L3_48_CLIENT;
DROP ROLE SYSTEM_CLIENT;
DROP ROLE L3_44_ARTISTE;


--Création des différents rôles
CREATE ROLE L3_48_CLIENT;
CREATE ROLE SYSTEM_CLIENT;
CREATE ROLE L3_44_ARTISTE;

--On commence en supprimant tout les droits d'accès des utilisateurs
REVOKE ALL ON CLIENTS FROM PUBLIC;
REVOKE ALL ON MUSICPOSS FROM PUBLIC;
REVOKE ALL ON ASSOCALBART FROM PUBLIC;
REVOKE ALL ON MUSIQUES FROM PUBLIC;
REVOKE ALL ON ALBUMS FROM PUBLIC;
REVOKE ALL ON ACHATS FROM PUBLIC;
REVOKE ALL ON COMPETENCES FROM PUBLIC;
REVOKE ALL ON GROUPES FROM PUBLIC;
REVOKE ALL ON ARTISTES FROM PUBLIC;

--Le Client ne peut mettre a jour que son mots de passe et son numéro de téléphone
GRANT UPDATE(Cl_passw, Cl_tel) ON CLIENTS TO L3_48_CLIENT;
GRANT UPDATE(Cl_passw, Cl_tel) ON CLIENTS TO SYSTEM_CLIENT;

-- L'artiste peut ajouter un album dans la base de donnée
GRANT INSERT ON ALBUMS TO L3_44_ARTISTE;
GRANT INSERT ON MUSIQUES TO L3_44_ARTISTE;
GRANT INSERT ON GROUPES TO L3_44_ARTISTE;

-- /!\ Pour restreindre les colonnes d'une table pour une requête SELECT d'un utilisateur, il faut passer par les vues car ce n'est pas légal au niveau du sql de faire la requête suivante : l'utilisateur ne peut effectuer que la requête SELECT sur les colonne Cl_pseudo et Cl_statut !
-- GRANT SELECT(Cl_pseudo, Cl_satut) ON CLIENT TO L3_48_CLIENT 

DROP VIEW V_CLIENT_INFO;
CREATE VIEW V_CLIENT_INFO AS
SELECT Cl_pseudo, Cl_statut
FROM CLIENTS;

GRANT SELECT ON V_CLIENT_INFO TO L3_48_CLIENT;
GRANT SELECT ON V_CLIENT_INFO TO SYSTEM_CLIENT;

--Le client peut effectuer des opérations SELECT sur les attributs définit ci-dessous dans la vues pour voir les caractéristique d'un album de la base de donnée !
DROP VIEW V_CLIENT_MUSIQUE_FOR_ROLE;
CREATE VIEW V_CLIENT_MUSIQUE_FOR_ROLE AS
SELECT Al_nom, Al_duree, M_titre, M_duree, G_nom, G_dateCrea, Art_nom, Art_prenom
FROM (((ALBUMS NATURAL JOIN MUSIQUES) NATURAL JOIN GROUPES) NATURAL JOIN ASSOCALBART) NATURAL JOIN ARTISTES;

GRANT SELECT ON V_CLIENT_MUSIQUE_FOR_ROLE TO L3_48_CLIENT;

--Le client peut voir ces achats !

CREATE OR REPLACE VIEW V_CLIENT_MUSIQUE_FOR_ROLE AS
SELECT M_titre
FROM MUSICPOSS
WHERE Cl_pseudo = (SELECT user FROM dual)
GROUP BY M_titre;

GRANT SELECT ON V_CLIENT_MUSIQUE_FOR_ROLE TO L3_48_CLIENT;
GRANT SELECT ON V_CLIENT_MUSIQUE_FOR_ROLE TO SYSTEM_CLIENT;

--Attribution des rôles aux personnes !
GRANT L3_48_CLIENT TO L3_48;
GRANT L3_44_ARTISTE TO L3_44;
