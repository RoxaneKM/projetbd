# Projet BD

----
## Ordre d'ex�cution des fichiers SQL:

    start creations
    start operations
    start insertions
    start tests       --facultif
    start vues        

/!\ Si tests.sql a �t� lanc� avant vues.sql, La vue retournera vide ce qui normal car j'ai supprim� des tuples dans test.sql afin de v�rifier le bon fonctionnement des triggers. 

Si la vue retourne vide, lancer:
    
    start insertions
    start vues

Et �a devrait marcher.

----
